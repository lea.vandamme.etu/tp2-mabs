from TableSuffixes import *
from Read import *
from Bio import SeqIO
import gzip


def reverse_complement(seq):
    """
    Create the reverse complement of a sequence

    :param seq: the sequence we want to transform
    :type seq: str
    :return: the reverse complement
    :rtype: str
    """
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    res = ''
    for i in range(len(seq)-1,-1,-1) :
        res += complement[seq[i]]
    return res

def readPourcentage(read, tableSuffixes, modes, seuil):
    """
    Give the pourcentage of subreads wich are present in the suffix array

    :param read: the read that we will study the subwords
    :type read: Read
    :param tableSuffixes: the suffix array
    :type tableSuffixes: list
    :param modes: the number of modes we want to use to create the subwords
    :type modes: int
    :param seuil: the threshold we fix to keep the reads or not
    :type seuil: int
    :return: the read if we want to keep it or 0 otherwise.
    :rtype: Read or int
    """
    total = 0
    wrong = 0
    read = Read(read.id,read.seq, read.name, read.description)
    subwords = read.cut_read(read.getSeq(), modes)
    lenSubword = len(subwords)
    for subword in subwords:
        if tableSuffixes.isPresent(str(subword)) or tableSuffixes.isPresent(str(reverse_complement(subword))):
            total += 1
            if (total * 100)/lenSubword >= seuil :
                return read
        else:
            wrong += 1
            if (wrong * 100)/lenSubword >= (1-seuil) :
                return 0
    return 0

def filterReads(fastq, modes, seuil, output):
    """
    Insert the reads corresponding to the covid genome.

    :param fastq: the read file
    :type fastq: str
    :param modes: the number of modes we want to use to create the subwords
    :type modes: int
    :param seuil: the threshold we fix to keep the reads or not
    :type seuil: int
    :param output: the output file
    :type output: str
    :return: Nothing
    :rtype: None
    """
    assert isinstance(int(modes),int) and isinstance(int(seuil),int), "La longueur des kmers et le seuil d'acceptabilité doivent être des nombres entiers."
    fastqFile = SeqIO.parse(gzip.open(fastq, "rt"), 'fastq')
    fastaCovid = SeqIO.read("fichiers/covid.fasta", "fasta")
    suffixesCovid = TableSuffixes(fastaCovid.seq) ; suffixesCovid.sortSuffixeTable()
    for elt in fastqFile:
        pct = readPourcentage(elt, suffixesCovid, int(modes), int(seuil))
        if isinstance(pct, Read) :
            with open(output, 'a') as handle:
                SeqIO.write(elt, handle, 'fastq')