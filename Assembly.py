from Read import *
from Bio import SeqIO
import gzip
import operator
from random import choice

def dico(reads, lenKmer, seuil):
    """
    Create the dictionnary containing all the kmers and their occurence

    :param reads: the name of the file containing the reads
    :type reads: str
    :param lenKmer: the length of the Kmers
    :type lenKmer: int
    :param seuil: the minimal occurence of the kmer to use it
    :type seuil: int
    :return: the dictionnary 
    :rtype: dict
    """
    dico = {}
    fastqFile = SeqIO.parse(gzip.open(reads, "rt"), 'fastq')
    for read in fastqFile:
        read = Read(read.id,read.seq, read.name, read.description)
        kmers = read.kmer(lenKmer)
        for kmer in kmers:
            if kmer in dico.keys():
                dico[str(kmer)][0] += 1
                if dico[str(kmer)][0] >= seuil:
                    dico[str(kmer)][1] = 1
            else : 
                dico[str(kmer)] = [1, 0]
    for kmer in list(dico):
        if dico[kmer][1] == 0:
            del dico[kmer]
    return dico

def frequences_nucl(reads):
    """
    Read a fastq file and give the nucleotide frequencies

    :param reads: the name of the read file
    :type reads: str
    :return: a dictionnary containing the frequencies
    :rtype: dict
    """
    frequences = {}
    fastqFile = SeqIO.parse(gzip.open(reads, "rt"), 'fastq')
    lenTotal = 0
    for read in fastqFile:
        lenTotal += len(read.seq)
        for nucl in read.seq :
            if nucl in frequences.keys():
                frequences[nucl] += 1
            else:
                frequences[nucl] = 1
    for k in frequences.keys():
        frequences[k] = (frequences[k]*100)/lenTotal
    return frequences
    

def nextKmer(dico, ref, lenKmer, nucleotide, cote):
    """
    Search in the dictionnary if we can enlarge the sequence by adding a nucleotide

    :param dico: the dictionnary containing the kmers
    :type dico: dict
    :param ref: the sequence we want to enlarge
    :type ref: str
    :param lenKmer: the length of the Kmers
    :type lenKmer: int
    :param nucleotide: the nucleotide we add to the reference sequence
    :type nucleotide: str
    :param cote: the side of the sequence we want to enlarge
    :type cote: str
    :return: the next kmer if it is present in the dictionnary
    :rtype: str
    """
    if cote == 'D':
        kmerToSearch = ref[-lenKmer+1:] + nucleotide
    elif cote == 'G':
        kmerToSearch = nucleotide + ref[:lenKmer-1] 
    for k in dico.keys():
        if k == kmerToSearch and dico[k][1] == 1 :
            return k
    return ''

def concatReads(ref, nucl, cote):
    """
    Concatenate a sequence and a new nucleotide

    :param ref: the sequence we want to enlarge
    :type ref: str
    :param nucl: the nucleotide we want to add
    :type nucl: str
    :param cote: the side of the sequence we want to enlarge
    :type cote: str
    :return: the new sequence
    :rtype: str
    """
    if cote == 'G':
        return nucl + ref
    else :
        return ref + nucl

def assemblageNaif(reads, lenKmer, seuil, output):
    """
    Take the filter reads and assemble them.

    :param reads: the name of the file containing the reads
    :type reads: str
    :param lenKmer: the length of the Kmers
    :type lenKmer: int
    :param seuil: the minimal occurence of the kmer to use it
    :type seuil: int
    :param output: the name of the resulting file
    :type output: str
    :return: the dictionnary 
    :rtype: dict
    """
    assert isinstance(int(lenKmer),int) and isinstance(int(seuil),int), "La longueur des kmers et le seuil d'acceptabilité doivent être des nombres entiers."

    dic = dico(reads, int(lenKmer), int(seuil))
    keys = dic.keys()
    nucleotides = ['A', 'T', 'C', 'G']
    total_length = 0
    
    for kmer in keys:

        stop = False
        stopD = False
        stopG = False

        if dic[kmer][1] == 1:
            ref = kmer
        else:
            continue

        #on élargit à gauche et à droite
        while not stop:
            tmpD = False
            tmpG = False

            #gauche
            if not stopG:
                tmpG = False
                lnucl = []
                for nucl in nucleotides :
                    match = nextKmer(dic, ref, int(lenKmer), nucl, 'G')
                    if match != '' :
                        lnucl.append((nucl, match))
                if len(lnucl) == 1 :
                    ref = concatReads(ref, lnucl[0][0], 'G')
                    dic[lnucl[0][1]][1] = 0
                    tmpG = True

            #droite
            if not stopD:
                tmpD = False
                lnucl = []
                for nucl in nucleotides :
                    match = nextKmer(dic, ref, int(lenKmer), nucl, 'D')
                    if match != '' :
                        lnucl.append((nucl, match))
                if len(lnucl) == 1 :
                    ref = concatReads(ref, lnucl[0][0], 'D')
                    dic[lnucl[0][1]][1] = 0
                    tmpD = True

            if not tmpG :
                stopG = True
            if not tmpD :
                stopD = True

            if stopG and stopD:
                stop = True

        with open(output, 'a') as results:
            if len(ref) > 500 :
                total_length += len(ref)
                results.write(ref + '\n')
    print(total_length)


def assemblageFrequences(reads, lenKmer, seuil, output):
    """
    Take the filter reads and assemble them.

    :param reads: the name of the file containing the reads
    :type reads: str
    :param lenKmer: the length of the Kmers
    :type lenKmer: int
    :param seuil: the minimal occurence of the kmer to use it
    :type seuil: int
    :param output: the name of the resulting file
    :type output: str
    :return: the dictionnary 
    :rtype: dict
    """
    assert isinstance(int(lenKmer),int) and isinstance(int(seuil),int), "La longueur des kmers et le seuil d'acceptabilité doivent être des nombres entiers."

    dic = dico(reads, int(lenKmer), int(seuil))
    keys = dic.keys()
    nucleotides = ['A', 'T', 'C', 'G']
    total_length = 0
    
    for kmer in keys:

        stop = False
        stopD = False
        stopG = False

        if dic[kmer][1] == 1:
            ref = kmer
        else:
            continue

        #on élargit à gauche et à droite
        while not stop:
            tmpD = False
            tmpG = False

            #gauche
            if not stopG:
                tmpG = False
                lnucl = []
                for nucl in nucleotides :
                    match = nextKmer(dic, ref, int(lenKmer), nucl, 'G')
                    if match != '' :
                        lnucl.append((nucl, match))
                if len(lnucl) == 1 :
                    ref = concatReads(ref, lnucl[0][0], 'G')
                    dic[lnucl[0][1]][1] = 0
                    tmpG = True
                elif len(lnucl) > 1 :
                    tmp = []
                    for elt in lnucl:
                        if elt[0] in ['A','T']:
                            tmp.append(elt)
                    if tmp != []:
                        if len(tmp) == 1:
                            ref = concatReads(ref, tmp[0][0], 'G')
                            dic[tmp[0][1]][1] = 0
                        else:
                            randomNucl = choice(tmp)
                            ref = concatReads(ref, randomNucl[0], 'G')
                            dic[randomNucl[1]][1] = 0
                        tmpG = True

            #droite
            if not stopD:
                tmpD = False
                lnucl = []
                for nucl in nucleotides :
                    match = nextKmer(dic, ref, int(lenKmer), nucl, 'D')
                    if match != '' :
                        lnucl.append((nucl, match))
                if len(lnucl) == 1 :
                    ref = concatReads(ref, lnucl[0][0], 'D')
                    dic[lnucl[0][1]][1] = 0
                    tmpD = True
                elif len(lnucl) > 1 :
                    tmp = []
                    for elt in lnucl:
                        if elt[0] in ['A','T']:
                            tmp.append(elt)
                    if tmp != []:
                        if len(tmp) == 1:
                            ref = concatReads(ref, tmp[0][0], 'D')
                            dic[tmp[0][1]][1] = 0
                        else:
                            randomNucl = choice(tmp)
                            ref = concatReads(ref, randomNucl[0], 'D')
                            dic[randomNucl[1]][1] = 0
                        tmpD = True

            if not tmpG :
                stopG = True
            if not tmpD :
                stopD = True

            if stopG and stopD:
                stop = True

        with open(output, 'a') as results:
            if len(ref) > 500 :
                total_length += len(ref)
                results.write(ref + '\n')
    print(total_length)
        


