class Read:
    """
    Class Read - Represent a read.
    id : The id of the read
    seq : the nucleotidic sequence
    name : the name of the read
    description : its description
    modes : the number of subwords we want to create to filter it
    """

    def __init__(self, id, seq, name, description):
        self.__id = id
        self.__seq = seq
        self.__name = name
        self.__description = description
        #self.__subword = self.cut_read(seq, modes)

    def getId(self):
        return self.__id

    def getSeq(self):
        return self.__seq

    def getName(self):
        return self.__name

    def getDescription(self):
        return self.__description

    def cut_read1(self, seq, modes):
        """
        Cut the read into a given number of modes.

        :param seq: the sequence of the read
        :type seq: str
        :param modes: the number of modes we want to create
        :type modes: int
        :return: a list of subwords
        :rtype: list
        """
        res = []
        sub_word = []
        etendue = len(seq)
        for i in range(modes):
            res.append(round(((i+1)/modes)*etendue))
        sub_word.append(seq[0:res[0]])
        for i in range(1,len(res)):
            sub_word.append(seq[res[i-1]:res[i]])
        return sub_word

    def cut_read(self, seq, longueur_read):
        """
        Cut the read into a given number of modes.

        :param seq: the sequence of the read
        :type seq: str
        :param modes: the number of modes we want to create
        :type modes: int
        :return: a list of subwords
        :rtype: list
        """
        lenSeq = len(seq)
        res = []
        for i in range(0,lenSeq,longueur_read):
            res.append(seq[i:i+longueur_read])
        return res

    def kmer(self,k):
        """
        Cut the read into kmers, having their length.
        
        :param k: the length of the kmers we want to create
        :type k: int
        :return: a dictionnary containing all the kmers and their occurance
        :rtype: dict
        """
        res = []
        for i in range(len(self.getSeq())-k+1):
            k_mer = self.getSeq()[i:i+k]
            res.append(k_mer)
        return res
