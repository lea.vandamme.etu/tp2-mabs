from functools import cmp_to_key

class TableSuffixes:
    """
    class TableSuffixes : a class representing a suffix array and its methods.
    seq : the sequence with wich we want to create the suffix array
    table : the suffix array
    """

    def __init__(self, seq, table = []):
        self.__seq = seq + '$'
        self.__table = []
    
    def getSeq(self):
        return self.__seq

    def getSuffixe(self, startPosition):
        return self.getSeq()[startPosition:]

    def getTable(self):
        return self.__table

    def setTable(self, table):
        self.__table = table

    def compare(self, pos1, pos2):
        """
        Comparison function used to sort the suffix array

        :param pos1: the start position of the first sequence to compare
        :type pos1: int
        :param pos2: the start position of the second sequence to compare
        :type pos2: int
        :return: -1, 0 or 1
        :rtype: int
        """
        if self.getSuffixe(pos1)[:-1] < self.getSuffixe(pos2)[:-1]:
            return -1
        elif self.getSuffixe(pos1)[:-1] > self.getSuffixe(pos2)[:-1]:
            return 1
        else:
            return 0
    
    def sortSuffixeTable(self):
        """
        A fonction to sort the suffix array in lexicographic order

        :return: a sorted suffix array
        :rtype:  list
        """
        l = [i for i in range(len(self.getSeq()))]
        res = sorted(l, key=cmp_to_key(self.compare))
        self.setTable(res)
        return res
    
    def isPresent(self, read):
        """
        Test if a read is present in the suffix array or not

        :param read: the read we want to search
        :type read: Read
        :return: True if the read is in the suffix array, False otherwise
        :rtype: Boolean
        """
        g = 0
        d = len(self.getTable())-1
        lenRead = len(read)
        while g <= d :
            m = (d+g)//2
            current = self.getTable()[m]
            tmp = True
            for i in range(lenRead):
                if current+i >= len(self.getSeq()) : 
                    return False
                else:
                    if read[i] != self.getSeq()[current+i]:
                        tmp = False
            if tmp :
                return True
            else:
                if self.getSeq()[current:current+lenRead] > read :
                    d = m-1
                elif self.getSeq()[current:current+lenRead] < read :
                    g = m+1
        return False


    
