import sys
from Filter import *
from Assembly import *
import time

if __name__ == "__main__":

    if len(sys.argv) != 6:
        print("Nombre de paramètres incorrects.")

    elif sys.argv[1] == '-filtre' :
        filterReads(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    elif sys.argv[1] == '-assemblageNaif':
        assemblageNaif(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
    elif sys.argv[1] == '-assemblageFrequences':
        assemblageFrequences(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

    else : 
        print("Premier paramètre incorrect : les 3 possibilités sont -filtre , -assemblageNaif , -assemblageFrequences")
