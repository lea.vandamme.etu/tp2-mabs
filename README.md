# TP2-MABS - Pellegri Tayna ; Vandamme Léa
## M1 MISO

## Contenu du dépôt

- Le script Python dans le répertoire principal
- un dossier resultats contenant :
1. Un fichier results.fastq.gz contenant le résultat de notre filtrage, utilisé pour les assemblages
2. Deux dossiers pour nos deux types d'assemblage : assemblage naif et en tenant en compte les fréquences. 
Dans chaque dossier, il y a 3 fichiers correspondants à 3 seuils d'occurrence minimale des Kmers (2, 3 et 4) dans notre structure de données.
3. Un dossier pour les résultats Bowtie2 que nous avons lancés après assemblage : 3 fichiers pour l'assemblage naif et 3 autres pour l'assemblage prenant en compte les fréquences.

## Lancement du code

#### Pour filtrer les reads venant probablement du covid : 

` $ python3 Main.py -filtre <fichier_contenant_les_reads> <longeur_des_sous_reads> <seuil_d'acceptabilité_des_reads> <chemin_du_fichier_résultat>
`

Exemple de commande :

` $ python3 Main.py -filtre fichiers/reads-1M.fastq.gz 20 20 resultats/covid.fastq
`

en utilisant donc le fichier fourni dans le sujet.

Avec : 

- le fichier_contenant_les_reads au format .fastq.gz

- la longueur des sous-reads est un entier (que nous avons fixé à 20 pour nos tests ici)

- le seuil d'acceptabilité est compris entre 0 et 100 ; il correspond au pourcentage de sous-reads trouvés dans la table des suffixes (nous l'avons fixé à 20 pour nos tests)

- le fichier contenant le résultat au format fastq.

#### Pour réaliser l'assemblage naif 

` $ python3 Main.py -assemblageNaif <fichier_contenant_les_reads> <longeur_des_kmers> <nombre_d'occurences_minimal_des_kmers> <chemin_du_fichier_résultat>
`

Exemple de commande : 

` $ python3 Main.py -assemblageNaif resultats/results.fastq.gz 30 2 resultats/testAssemblage.txt
`

Avec : 

- le fichier_contenant_les_reads au format .fastq.gz

- la longueur des kmers est un entier (que nous avons fixé à 30 pour nos tests ici)

- le nombre d'occurences minimal des kmers est un entier ; il correspond au pourcentage de sous-reads trouvés dans la table des suffixes (nous l'avons fixé à 2 pour le test ici)

- le fichier contenant le résultat au format txt

#### Pour réaliser l'assemblage prenant en compte les fréquences

` $ python3 Main.py -assemblageFrequences <fichier_contenant_les_reads> <longeur_des_kmers> <nombre_d'occurences_minimal_des_kmers> <chemin_du_fichier_résultat>
`

Exemple de commande : 

` * python3 Main.py -assemblageFrequences resultats/results.fastq.gz 30 2 resultats/testAssemblageFreq.txt
`

## Nos structures de données

### Filtrage

Nous avons utilisé une table des suffixes, en ne stockant que les positions des suffixes.
Les mots contenus dans cette structures sont des Kmers non chevauchants de longueur fixe qui représentent les reads.

La recherche dans cette table est réalisée par dichotomie.

### Assemblage

Nous avons utilisé un dictionnaire Python pour stocker les K mers construits à partir des reads filtrés.

La structure de nos clés/valeurs est :
- clé : le kmer 
- valeur : liste contenant le nombre d'occurences et un flag indiquant si le kmer a déjà été pris ou non.

#### Assemblage naif

Assemblage qui utilise le principe du graphe de De Bruijn : nous essayons d'ajouter un nucléotide à la fois en cherchant le kmer suivant dans notre dictionnaire. S'il y a plusieurs possibilités, nous stoppons.

#### Assemblage prenant en compte les fréquences des nucléotides

C'est le même principe que l'assemblage naif, à la différence que lorsque nous avons plusieurs possibilités pour prolonger le contig, nous choisissons celui qui a la plus grande fréquence.

Ici, nous avons comme fréquences : A et T -> 31% chacun et C et G -> 19% chacun.

Nous privilégions donc les A et les T, et si nous avons le choix entre les deux pour l'instant nous faisons au hasard.
